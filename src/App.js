import React, { Component } from "react";
import logo from "./logo.svg";
import { Button, Alert, Input } from "reactstrap";
import CoinFlipComponent from "./CoinFlipComponent";
import { getCoinFlip } from "./coinflipService";

import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      outcome: " ",
      coinImage: " ",
      outcomePercentage: " ",
      individualFlips: [],
      id: "1"
    };

    //making sure that these functions can interact with state
    this.setId = this.setId.bind(this);
    this.getCoinFlip = this.getCoinFlip.bind(this);
  }

  getCoinFlip() {
    getCoinFlip(this.state.id).then(coinFlip => {
      this.setState({
        outcome: coinFlip.outcome,
        coinImage: coinFlip.coinImage,
        outcomePercentage: coinFlip.outcomePercentage,
        individualFlips: coinFlip.individualFlips,
        isLoaded: true
      });
    });
  }

  /**
   * called by input on change.  Sets the is
   * @param {event handler} event
   */
  setId(event) {
    this.setState({ id: event.target.value });
  }

  render() {
    let alert =
      this.state.id < 1 || this.state.id > 29 || this.state.id % 2 == 0 ? (
        <Alert color="danger">
          Please enter a odd number between 1 and 29.
        </Alert>
      ) : (
        //return {Alert};
        ""
      );

    let output = this.state.isLoaded ? (
      <CoinFlipComponent
        outcome={this.state.outcome}
        //individualFlips.forEach(flip=> { flipArray.push(<CoinFlipComponent side=this._side coinImage=this._coinImage, flipNumber=this._flipNumber }>)
        coinImage={this.state.coinImage}
        individualFlips={this.state.individualFlips}
        flipCoin={this.state.flipCoin}
        outcomePercentage={this.state.outcomePercentage}
      />
    ) : (
      ""
    );

    return (
      <div className="App">
        <header className="App-header">
          <span>Coin Flip Ui</span>

          <Input
            className="flip-input"
            onChange={this.setId}
            type="number"
            min="1"
            max="29"
            placeholder="Number of flips"
          />
          {/* <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6X4fWrtH4M9-33Sqz_Tb0clc1yMv0R34_c-tjoE7iWB8V5ctUMg" /> */}
          {/* <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a> */}
          <div> Created by: Stephanie Maynard </div>
          <Button onClick={this.getCoinFlip} color="primary">
            Click to Flip Coin
          </Button>
          {alert}
          {output}
        </header>
      </div>
    );
  }
}

export default App;
