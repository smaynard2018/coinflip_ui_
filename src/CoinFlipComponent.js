import React, { Component } from "react";

export default class CoinFlipComponent extends Component {
  constructor(props) {
    super(props); //constructor & super calls w/ props argument are required for props to work
  }

  render() {
    //   return multiline jsx objects in parentheses
    // must have one parent / object in your return

    //create array here
    let flipArray = [];
    console.log(this.props.individualFlips);
    this.props.individualFlips.forEach(flip => {
      console.log("individual coin image:" + flip.image);
      flipArray.push(
        <div>
          <img src={flip.image} />
        </div>
      );
    });
    

    console.log("outcome percentage " + this.props.outcomePercentage);
    return (
      <div>
        <div>outcome: {this.props.outcome}</div>
        <div>coinImage: <img src={this.props.coinImage}/></div>
        <div>outcomePercentage: {this.props.outcomePercentage}</div>
        <div>individualFlips: {flipArray}</div>
      </div>
    );
  }
}

//{"outcome":"heads","coinImage":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6X4fWrtH4M9-33Sqz_Tb0clc1yMv0R34_c-tjoE7iWB8V5ctUMg","outcomePercentage":"80.00","individualFlips":[{"side":"heads","coinImage":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6X4fWrtH4M9-33Sqz_Tb0clc1yMv0R34_c-tjoE7iWB8V5ctUMg","flipNumber":1},{"side":"heads","coinImage":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6X4fWrtH4M9-33Sqz_Tb0clc1yMv0R34_c-tjoE7iWB8V5ctUMg","flipNumber":2},{"side":"tails","coinImage":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6X4fWrtH4M9-33Sqz_Tb0clc1yMv0R34_c-tjoE7iWB8V5ctUMg","flipNumber":3},{"side":"heads","coinImage":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6X4fWrtH4M9-33Sqz_Tb0clc1yMv0R34_c-tjoE7iWB8V5ctUMg","flipNumber":4},{"side":"heads","coinImage":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6X4fWrtH4M9-33Sqz_Tb0clc1yMv0R34_c-tjoE7iWB8V5ctUMg","flipNumber":5}]}
