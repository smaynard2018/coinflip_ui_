import rp from "request-promise";

export const getCoinFlip = numTimes => {
  let options = {
    method: "GET",
    uri: "https://sneupane-coinflip.herokuapp.com/coinflip/" + numTimes,
    json: true
  };
  return rp(options);
};
